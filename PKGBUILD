# Maintainer: Robin Candau <antiz@archlinux.org>
# Contributor: kpcyrd <kpcyrd[at]archlinux[dot]org>
# Contributor: Piotr Miller <nwg.piotr@gmail.com>

pkgname=nwg-look
pkgver=1.0.3
pkgrel=1
pkgdesc="GTK settings editor adapted to work on wlroots-based compositors"
url="https://github.com/nwg-piotr/nwg-look"
arch=('x86_64')
license=('MIT')
depends=(
  'glibc'
  'gtk3'
  'libatk-1.0.so'
  'libcairo-gobject.so'
  'libcairo.so'
  'libfontconfig.so'
  'libfreetype.so'
  'libgdk-3.so'
  'libgdk_pixbuf-2.0.so'
  'libgio-2.0.so'
  'libglib-2.0.so'
  'libgmodule-2.0.so'
  'libgobject-2.0.so'
  'libgtk-3.so'
  'libharfbuzz.so'
  'libpango-1.0.so'
  'libpangocairo-1.0.so'
  'libz.so'
  'xcur2png'
)
makedepends=(
  'go'
)
source=(https://github.com/nwg-piotr/${pkgname}/archive/v${pkgver}/${pkgname}-${pkgver}.tar.gz)
sha256sums=('bab42b80ae6d5ce4ac11ed563e71c4f54608d1e97272001dd6494bd92d471177')
b2sums=('6e0b1390d5376ce8d8ddbff2d55fa4efd089332b6d61fe0d44f6bfb2ed6890205e7fa9755fce3039c09c0f1da2f40de1ff74f2cd9434ead2548c1e69c497abb8')

build() {
  cd ${pkgname}-${pkgver}
  go build \
    -trimpath \
    -buildmode=pie \
    -mod=readonly \
    -modcacherw \
    -ldflags "-linkmode external -extldflags \"${LDFLAGS}\"" \
    -o bin/nwg-look \
    .
}

package() {
  make DESTDIR="${pkgdir}" install -C "${pkgname}-${pkgver}"
}

# vim: ts=2 sw=2 et:
